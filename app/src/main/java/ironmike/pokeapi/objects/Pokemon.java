package ironmike.pokeapi.objects;

/**
 * Created by miken on 12/3/15.
 */
public class Pokemon {
    String name;
    String reourceURI;

    public Pokemon(String name) {
        this.name = name;
    }

    public Pokemon(String name, String reourceURI) {
        this.name = name;
        this.reourceURI = reourceURI;
    }

    public String getName() {
        return name;
    }

    public String getReourceURI () {
        return reourceURI;
    }

    public void setReourceURI(String reourceURI) {
        this.reourceURI = reourceURI;
    }

    public String toString() {
        return ("Pokemon: " + name + " URL: " + reourceURI);
    }
}
