package ironmike.pokeapi.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ironmike.pokeapi.objects.Pokemon;
import ironmike.pokeapi.R;

/**
 * Created by miken on 12/3/15.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.PokemonViewHolder> {
    public List<Pokemon> pokemon;

    public static class PokemonViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView pokemonName;

        PokemonViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView)itemView.findViewById(R.id.card_view);
            pokemonName = (TextView)itemView.findViewById(R.id.pokemon_name);
        }
    }

    public RecyclerViewAdapter(List<Pokemon> pokemon) {
        this.pokemon = pokemon;
    }

    @Override
    public int getItemCount() {
        return pokemon.size();
    }



    @Override
    public PokemonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_layout, viewGroup, false);
        PokemonViewHolder pokemonViewHolder = new PokemonViewHolder(view);
        return pokemonViewHolder;
    }

    @Override
    public void onBindViewHolder(PokemonViewHolder pokemonViewHolder, int i) {
        pokemonViewHolder.pokemonName.setText(pokemon.get(i).getName());

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}
