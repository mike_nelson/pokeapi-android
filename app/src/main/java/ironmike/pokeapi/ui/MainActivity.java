package ironmike.pokeapi.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import ironmike.pokeapi.R;
import ironmike.pokeapi.adapters.ItemClickSupport;
import ironmike.pokeapi.adapters.RecyclerViewAdapter;
import ironmike.pokeapi.objects.Pokemon;

public class MainActivity extends AppCompatActivity {

    private List<Pokemon> pokemonList;
    private JSONArray pokeArray;

    protected RecyclerView recyclerView;
    RecyclerViewAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        //Set up UI Elements
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        //Set up data
        pokemonList = new ArrayList<>();
        initializeData();

        //RecyclerView
        adapter = new RecyclerViewAdapter(pokemonList);
        recyclerView.setAdapter(adapter);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener(){
            @Override
            public void onItemClicked(RecyclerView rv, int position, View v) {
                Toast.makeText(getApplicationContext(), "Selected: " + pokemonList.get(position).getName(), Toast.LENGTH_LONG).show();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // TODO Split the logic up into classes as needed.

    private void initializeData() {

        Thread thread = new Thread (new Runnable() {
            @Override
            public void run() {
                try {
                    pokeArray = getPokeArray(apiCall());
                    addPokemonToList(pokeArray);
                } catch (IOException e) { e.printStackTrace();}
            }


        });
        thread.start();



    }

    private String apiCall() throws IOException {
        String pokedexUrl = "http://pokeapi.co/api/v1/pokedex/1/";
        URL url = new URL(pokedexUrl);

        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();

        httpCon.setRequestMethod("GET");
        httpCon.connect();
        int responseCode = httpCon.getResponseCode();
        Log.d("MainActivity", "Response Code: " + responseCode);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
        String input;
        StringBuffer response = new StringBuffer();

        while ((input = bufferedReader.readLine()) != null) {
            response.append(input);
        }

        input = "" + response;

        //Closing the Connection
        bufferedReader.close();
        httpCon.disconnect();

        return input;

    }

    private JSONArray getPokeArray(String inputString) {
        JSONArray pokeArray = null;
        JSONObject object;
        try {
            object = new JSONObject(inputString);
            //Log.d("MainActivity", ""+object.get("pokemon"));
            pokeArray = object.getJSONArray("pokemon");
            //Log.d("MainActivity", ""+pokeArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return pokeArray;
    }

    private void addPokemonToList(JSONArray pokeArray) {
        for(int i = 0; i < pokeArray.length(); i++) {
            try {

                JSONObject tmpJSONObj = new JSONObject(pokeArray.getString(i));

                Pokemon tmpPokemon = new Pokemon(tmpJSONObj.getString("name"), tmpJSONObj.getString("resource_uri"));
                pokemonList.add(tmpPokemon);
                //Log.d("MainActivity", tmpPokemon.toString());
            } catch (JSONException e) { e.printStackTrace(); }
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

}
